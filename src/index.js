import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './responsive.css'
import App from './App.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import 'swiper/swiper.scss';
// import 'react-awesome-button/dist/themes/theme-blue.css';
import "react-awesome-button/dist/styles.css";
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import Reducer from './redux/reducers/loginReducer';
import { BrowserRouter as Router } from "react-router-dom";
// import reportWebVitals from './reportWebVitals';

const store = createStore(Reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
