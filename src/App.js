import React, { useEffect, useState } from "react";
import { Route, Switch, useLocation } from "react-router-dom";
import Home from "./components/Home";
import Login from "./components/Login";
import { useDispatch } from "react-redux";
import VideoFromCommander from "./components/VideoFromCommander";
import Welcome from "./components/Welcome";
import { loginAction } from "./redux/actions/loginAction";
import CheckOrientation from "./components/CheckOrientation";
import Tutorial from "./components/Tutorial";
import CommitmentRoom from "./components/CommitmentRoom";
import Conference from "./components/Conference";
import VideoWall from "./components/VideoWall";
import Agenda from "./components/Agenda";
import Spaceship from "./components/Spaceship";
import { AnimatePresence } from "framer-motion";
import Tampilan from "./components/Tampilan";

function App() {
  const dispatch = useDispatch();
  const [loading, setloading] = useState(true);
  const location = useLocation();
  console.log(location);

  useEffect(() => {
    const data = JSON.parse(localStorage.getItem("asc"));
    // console.log(data)
    if (data) {
      dispatch(
        loginAction({
          id: data.id,
          nik: data.nik,
          name: data.name,
          firstLogin: data.firstLogin,
          token: data.token,
          confirmLogin: data.confirmLogin,
          agenda: data.agenda,
        })
      );
    }
    setloading(false);
    // const script = document.createElement('script');
    //   script.src = "/helper/Main.js/";
    //   script.async = true;
    //   document.body.appendChild(script);
    //   return () => {
    //   document.body.removeChild(script);
    //   }
  }, []);

  return loading ? (
    <h1 className="text-center" style={{ color: "black" }}>
      L O A D I N G ...
    </h1>
  ) : (
    <AnimatePresence exitBeforeEnter>
      <Switch location={location} key={location.pathname}>
        <Route path="/" exact component={Login} />
        {/* <Route
          path="/"
          exact
          component={() => {
            window.location = "https://competition.asc2021.id/";
            return null;
          }}
        /> */}
        <Route path="/home" component={Home} />
        <Route path="/commitmentroom" component={CommitmentRoom} />
        <Route path="/videoroom" component={VideoWall} />
        <Route path="/checkOrientation" component={CheckOrientation} />
        <Route path="/videoFromCommander" component={VideoFromCommander} />
        <Route path="/tutorial" component={Tutorial} />
        <Route path="/agenda" component={Agenda} />
        <Route path="/welcome" component={Welcome} />
        <Route path="/conference" component={Conference} />
        <Route path="/spaceship" component={Spaceship} />
        <Route
          path="/tampilanuntukditampilkanolehkodeodiacaraascbayeryangtidakberkepentingandilarangmasuk19283746556473829"
          component={Tampilan}
        />
      </Switch>
    </AnimatePresence>
  );
}

export default App;
