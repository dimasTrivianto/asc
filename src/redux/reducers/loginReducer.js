const INITIAL_STATE = {
    id: '',
    nik: "",
    name: "",
    firstLogin:"",
    token:"",
    login: false,
    confirmLogin: false,
    loading: false,
    error:false,
    alreadyLogin: false,
    agenda: "",
    // check: false,
  };

export default (state = INITIAL_STATE, action) => {
    // console.log(action.payload)
    switch (action.type) {
        case "LOGIN_SUCCESS":
            return {
                ...state,
                ...action.payload,
                login:true,
                loading:false
            }
        case "LOGIN_FAILED":
            return {
                ...state,
                error:true,
                loading:false
            }
        case "CONFIRM_LOGIN":
            return {
                ...state,
                confirmLogin: true
            }
        case "LOADING":
            return {
                ...state,
                loading:true
            }
        case "FIRST_LOADING":
            return {
                ...state,
                firstLogin : action.payload
            }
        // case "CHECK":
        //     return {
        //         ...state,
        //         check : action.payload.check
        //     }
        case "AGENDA":
            return {
                ...state,
                agenda : action.payload.agenda
            }
        case "LOGOUT":
            return INITIAL_STATE
        default:
            return state
    }
}