export const loginAction = (data) => {
    console.log(data)
    localStorage.setItem("asc", JSON.stringify(data));
    return {
        type: "LOGIN_SUCCESS",
        payload: data
    }
}

export const logoutAction = () => {
    localStorage.removeItem("asc");
    return {
        type: "LOGOUT"
    }
}
