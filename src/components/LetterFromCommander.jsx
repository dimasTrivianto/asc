import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { WindupChildren } from "windups";
import { useSelector } from "react-redux";

export default function LetterFromCommander() {
  const [moving, setmoving] = useState(false);
  const firstLogin = useSelector((state) => state.firstLogin);

  useEffect(() => {
    if (firstLogin === false) {
      setmoving(true);
    }
  }, []);

  const finish = () => {
    setmoving(true);
  };

  return moving ? (
    <Redirect to="/tutorial" />
  ) : (
    <div className="container-fluid full-width ">
      <div className="main-background">
        <div className="col main-display-video">
          <div>
            <button type="button" class="btn btn-danger btn-log-out">
              <i className="fas fa-sign-out-alt"></i>
            </button>
          </div>
          <div>
            <h5 className="letter-title">Letter From Our Commander</h5>
            <div className="text-content">
              <WindupChildren onFinished={finish}>
                <p>
                  {
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque lectus sapien, facilisis ut odio non, venenatis fermentum nunc. Proin dolor metus, egestas ut accumsan vel, facilisis sed magna. Morbi lacinia tincidunt ultricies. Nullam dapibus eros id libero consectetur volutpat. Praesent posuere leo velit, in sollicitudin orci hendrerit nec. Aenean libero sapien, fringilla sit amet turpis scelerisque, ullamcorper porttitor est. Morbi aliquam eros in diam tempus volutpat. Duis pellentesque fringilla nunc eget varius. Duis in viverra felis, porttitor gravida urna. Donec tristique augue sed lobortis scelerisque. Nam quis posuere diam. Ut vehicula ac justo in porttitor. Nam mattis ut enim blandit efficitur. Morbi pretium dolor a ullamcorper tristique. Proin sapien leo, fringilla sit amet fringilla at, malesuada ut odio."
                  }
                </p>
                <p>
                  {
                    "Quisque commodo a dolor vitae vulputate. Vivamus quis nisl diam. Fusce tempor bibendum urna sed porttitor. Mauris id sapien id lorem imperdiet sodales. Suspendisse ullamcorper sit amet orci a mattis. Proin sagittis blandit nisl ac lobortis. Aliquam dapibus, nulla luctus maximus mattis, nisl lacus rhoncus elit, interdum pharetra ex ipsum et sapien. Sed ultrices mi sed convallis vehicula. Morbi tincidunt justo velit, vitae volutpat justo varius sed. Curabitur at libero elit. Suspendisse quis aliquet urna, at maximus justo. In dolor ex, accumsan vitae pulvinar non, ultricies nec enim. Duis vitae vestibulum lectus. Proin finibus fermentum vehicula."
                  }
                </p>
                <p>
                  {
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque lectus sapien, facilisis ut odio non, venenatis fermentum nunc. Proin dolor metus, egestas ut accumsan vel, facilisis sed magna. Morbi lacinia tincidunt ultricies. Nullam dapibus eros id libero consectetur volutpat. Praesent posuere leo velit, in sollicitudin orci hendrerit nec. Aenean libero sapien, fringilla sit amet turpis scelerisque, ullamcorper porttitor est. Morbi aliquam eros in diam tempus volutpat. Duis pellentesque fringilla nunc eget varius. Duis in viverra felis, porttitor gravida urna. Donec tristique augue sed lobortis scelerisque. Nam quis posuere diam. Ut vehicula ac justo in porttitor. Nam mattis ut enim blandit efficitur. Morbi pretium dolor a ullamcorper tristique. Proin sapien leo, fringilla sit amet fringilla at, malesuada ut odio."
                  }
                </p>
                <p>
                  {
                    "Quisque commodo a dolor vitae vulputate. Vivamus quis nisl diam. Fusce tempor bibendum urna sed porttitor. Mauris id sapien id lorem imperdiet sodales. Suspendisse ullamcorper sit amet orci a mattis. Proin sagittis blandit nisl ac lobortis. Aliquam dapibus, nulla luctus maximus mattis, nisl lacus rhoncus elit, interdum pharetra ex ipsum et sapien. Sed ultrices mi sed convallis vehicula. Morbi tincidunt justo velit, vitae volutpat justo varius sed. Curabitur at libero elit. Suspendisse quis aliquet urna, at maximus justo. In dolor ex, accumsan vitae pulvinar non, ultricies nec enim. Duis vitae vestibulum lectus. Proin finibus fermentum vehicula."
                  }
                </p>
              </WindupChildren>
            </div>
          </div>
        </div>
        <div className="blanket"></div>
      </div>
    </div>
  );
}
