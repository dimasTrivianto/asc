import React, { useEffect } from "react";
import { motion } from "framer-motion";
import Ellipse from "../assets/images/Ellipse_blue.svg";
import { Link } from "react-router-dom";
import planeSound from "../assets/jet_sound_1.mp3";
import door1 from "../assets/door_sound1.mp3";

function Spaceship() {
  let audio = new Audio(planeSound);
  let audio1 = new Audio(door1);

  useEffect(() => {
    let timeoutPlane = setTimeout(() => {
      audio.play();
    }, 3000);
    return () => clearTimeout(timeoutPlane);
  }, []);

  const soundplay = () => {
    audio1.play();
  };

  const containerVariant = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: { duration: 1 },
    },
    exit: {
      opacity: 0,
      transition: { duration: 1 },
    },
  };

  return (
    <motion.div
      variants={containerVariant}
      initial="hidden"
      animate="visible"
      exit="exit"
    >
      {/* <iframe src={planeSound} allow="autoplay" id="audio" style={{display:"none"}}></iframe>
      <audio id="player" autoplay loop>
        <source src={planeSound} type="audio/mp3" />
      </audio> */}
      <div class="stars"></div>
      <div class="twinkling" style={{ overflow: "hidden" }}>
        <motion.div
          initial={{ x: -2400, y: 500 }}
          animate={{ x: 0, y: 0 }}
          transition={{ delay: 1, duration: 2, ease: "easeInOut" }}
          className="spaceship"
        >
          <Link to="/home">
            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              transition={{ delay: 3, duration: 2 }}
              className="spaceship-btn"
            >
              <div
                className="commitment-bold1 Blink"
                onClick={() => soundplay()}
              ></div>
              <img
                src={Ellipse}
                className=" notes-link"
                alt="Notes Room"
                style={{ marginBottom: "15px" }}
              />
              <h5 className="blue-text-bg">Enter Spaceship</h5>
            </motion.div>
          </Link>
        </motion.div>
      </div>
    </motion.div>
  );
}

export default Spaceship;
