import React from "react";
import {ProgressBar} from 'react-bootstrap'

function Loading() {
  // const [now, setnow] = useState('')
  
  // useEffect(() => {
  //   const interval = setInterval(() => {
  //     console.log('This will run every second!');
  //   }, 1000);
  //   return () => clearInterval(interval);
  // }, []);

  return (
    <>
      <div className="main-background">
        <div className="centered">
          <h3>Preparing your best experience</h3>
          {/* <ProgressBar now={now} label={`${now}%`} /> */}
        </div>
      </div>
    </>
  );
}

export default Loading;
