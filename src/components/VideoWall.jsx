import React, { useState, useEffect } from "react";
import Ellipse from "../assets/images/Ellipse_blue.svg";
import axios from "axios";
import { APIURL } from "../helper/Api";
import x from "../assets/images/x.png";
// import Back from "../assets/images/Back.svg";
import Back from "../assets/images/backButton.svg";
import { useSelector, useDispatch} from "react-redux";
// import { AwesomeButton } from 'react-awesome-button';
import { Link } from "react-router-dom";
import room_label from "../assets/images/room_label.png";
import { motion } from "framer-motion";
import { Redirect } from "react-router-dom";
import door1 from "../assets/door_sound1.mp3";
import { AwesomeButton } from "react-awesome-button";
import poster from "../assets/images/tesPoster.png"


export default function VideoWall() {
  const [popUp, setPopUp] = useState("");
  const [modal, setmodal] = useState("");
  const [length, setlength] = useState(0)
  const [textEffect, settextEffect] = useState("");
  const login = useSelector((state) => state.login);
  // const nik = useSelector((state) => state.nik);
  // const name = useSelector((state) => state.name);
  const token = useSelector((state) => state.token);
  // const id = useSelector((state) => state.id);
  const [videos, setvideos] = useState([]);
  let audio = new Audio(door1);
  const dispatch = useDispatch();



  useEffect(() => {
    if (token) {
      getVideo();
    }
  }, []);

  const soundplay= () => {
    audio.play();
  }

  //User Logout
  const logout = () => {
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    axios
      .post(`${APIURL}/api/logout`, {}, config)
      .then((res) => {
        localStorage.removeItem("asc");
        dispatch({
          type: "LOGOUT",
        });
      })
      .catch((err) => {
        console.log("gagal logout");
      });
  };

  const getVideo = () => {
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    axios
      .get(`${APIURL}/api/video`, config)
      .then((res) => {
        setvideos(res.data.success.video);
        console.log(res.data.success.video)
        // setlength(res.data.success.video.length)
      })
      .catch((err) => console.log({ err }));
  };

  const unfold = () => {
    setPopUp("one");
    setmodal("modal-active");
    // setRoom(params);
    settextEffect("textEffect");
  };

  const folding = () => {
    setPopUp("out");
    setmodal("");
    settextEffect("");
  };

  const renderVideo = () => {
    if (videos.length) {
      return videos.map((vid) => {
        // console.log(vid)
        // console.log(length)
        return (
          <div className="slider-wrapper sc" key={vid.id} >
            
            <video type="video/mp4" controls poster={"http://backend-asc2021.yokesen.com/" + vid.thumbnail}>
              <source src={"http://backend-asc2021.yokesen.com/" + vid.url} />
            </video>
            {vid.userLike === 1 ? (
              <button
                className="like-button"
                onClick={() => count(vid.id, vid.nik)}
              >
                <i class="fas fa-star"></i>
                <span className="counter">{vid.totalLike}</span>
              </button>
            ) : (
              <button
                className="like-button"
                onClick={() => count(vid.id, vid.nik)}
              >
                <i className="far fa-star"></i>
                <span className="counter">{vid.totalLike}</span>
              </button>
            )}
          </div>
        );
      });
    }
  };

  const count = (video_id, nik) => {
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    const data = {
      user_id: nik,
      video_id: video_id,
    };
    axios
      .post(`${APIURL}/api/like`, data, config)
      .then((res) => {
        console.log(res.data)
        getVideo();
      })
      .catch((err) => {
        console.log({ err });
      });
  };

  // const left = () => {
  //   setlength(length + 12.5)
  // }

  const containerVariant = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: { duration: 3 },
    },
    exit: {
      opacity: 0,
      transition: { duration: 3 },
    },
  };

  // Redirect to login page if user logout or not logged in
  if (login === false || login === "") {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/" />
      </motion.Fragment>
    );
  }

  return (
    <motion.div
      variants={containerVariant}
      initial="hidden"
      animate="visible"
      exit="exit"
      className={`video-room ${modal}`}
    >
      {/* <div className="blanket"></div> */}
      <div className="back" onClick={() => soundplay()}>
        <Link to="/home">
          <img src={Back} alt="back_button" className="back_button" />
        </Link>
      </div>
      <div>
        <img
          src={room_label}
          alt="conference room"
          className="room-label-img"
        />
        <h3 className="room-label">Video Room</h3>
      </div>
      <div className="logout-video">
          <AwesomeButton
            type="secondary"
            style={{ fontSize: "28px" }}
            onPress={() => {
              logout();
              soundplay();
            }}
          >
          <i className="fas fa-sign-out-alt"></i>
        </AwesomeButton>
      </div>
      <div
        className="video-entrance"
        onClick={() => {
          unfold(); soundplay()
        }}
      >
        
        <div className="video-bold Blink"></div>
        <img
          src={Ellipse}
          className=" wall-link"
          alt="Video Competition Room"
          style={{ marginBottom: "15px" }}
        />
        <h5 className="blue-text-bg">Winning Star Video Competition</h5>
        {/* <p>
            Let's look at creative videos <br /> created by our colleagues
          </p> */}
      </div>

      {/* modal */}
      <div id="modal-container" className={`${popUp}`}>
        <div className="modal-background">
          <div className="modal">
            <div className="container-fluid full-width">
              <div className="centering-bg">
                <div className="col main-display">
                  <img
                    src={x}
                    alt="close"
                    className="x-image"
                    onClick={() => {folding(); soundplay()}}
                  />
                  <div className={`text-light text-content-zoom ${textEffect}`}>
                    <div
                      className="slider-wrapper-outter"
                      id="slider-wrapper-outter"
                    >
                      <div className="slides-wrapper">
                        <div className="items" id="items">
                          {renderVideo()}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col bottom-display">
                  <div className="wrapper-text-bottom">
                    <h1 className={`text-light title-bottom ${textEffect}`}>
                      Anda berada di Ruang Winning Star Competition
                    </h1>
                    <p className={`text-light text-bottom ${textEffect}`}>
                      Produktivitas kita mungkin terbatas, namun tak ada batasan untuk kreativitas.
                      <br />
                      <br />
                      Seperti yang dibuktikan oleh rekan kita dalam video-video ini yang mengundang senyum kita :{'\)'}
                      <br /> 
                      <br /> 
                      Dukung dan berikan like pada video yang paling Anda sukai!
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
}
