import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { AwesomeButton } from "react-awesome-button";
import { motion } from "framer-motion";
import door1 from "../assets/door_sound1.mp3";

export default function Tutorial() {
  const name = useSelector((state) => state.name);
  const firstLogin = useSelector((state) => state.firstLogin);
  const [moving, setmoving] = useState(false);
  const login = useSelector((state) => state.login);
  let audio = new Audio(door1);

  useEffect(() => {
    if (firstLogin === "false") {
      setmoving(true);
    }
  }, []);

  const soundplay = () => {
    audio.play();
  };

  // Redirect to login page if user logout or not logged in
  if (login === false || login === "") {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/" />
      </motion.Fragment>
    );
  }

  // tulisan admin di pepet ke atas, button di absolute
  return moving ? (
    <motion.Fragment exit="undefined">
      <Redirect to="/agenda" />
    </motion.Fragment>
  ) : (
    <div className="container-fluid full-width ">
      <div className="main-background">
        <div className="col main-display-tutorial">
          <div className="">
            <h5 className="tutorial-title">
              Welcome aboard {name}!
            </h5>
            <div className="text-content-tutorial">
              <p>
              2020 adalah tahun yang penuh tantangan, namun kita terus beradaptasi untuk bertumbuh. 
              <br />
              Dengan ketangguhan sebagai tim, kita siap memulai Pharma ASC 2021.
              <br />
              Mari eksekusi strategi baru kita di tahun ini

              </p>
              <p>
                Let’s Win through Execution and Digital for Double Digit Growth!
              </p>
            </div>

            <motion.Fragment exit="undefined">
              <Link to="/videoFromCommander">
                <AwesomeButton
                  onPress={() => soundplay()}
                  style={{
                    width: "10vw",
                    height: "5vh",
                    zIndex: "12",
                    letterSpacing: "1px",
                    fontWeight: "400",
                    fontSize: "24px",
                    marginRight: "20px",
                    marginLeft: "40%",
                    position: "absolute",
                    top: "86%",
                    left: "4vw",
                  }}
                >
                  NEXT
                </AwesomeButton>
                {/* <button type="button" className="text-light btn tutorial-btn">
                NEXT
              </button> */}
              </Link>
            </motion.Fragment>
          </div>
        </div>
        {/* <div className="blanket"></div> */}
      </div>
    </div>
  );
}
