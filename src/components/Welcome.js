import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Redirect } from "react-router-dom";
import { AwesomeButton } from "react-awesome-button";
// import { isBrowser, isMobile } from "react-device-detect";
import { motion } from "framer-motion";
import door1 from "../assets/door_sound1.mp3";


function Welcome() {
  const confirmLogin = useSelector((state) => state.confirmLogin);
  const firstLogin = useSelector((state) => state.firstLogin);
  const login = useSelector((state) => state.login);
  const agenda = useSelector((state) => state.agenda);
  const dispatch = useDispatch();
  let audio = new Audio(door1);
  let width = window.innerWidth;
  // const tanggal = new Date().toLocaleDateString();
  const waktu = +new Date().getHours();

  useEffect(() => {
    var myValue = JSON.parse(localStorage["asc"]);
    // console.log(myValue);
  });

  const setLogin = () => {
    var myValue = JSON.parse(localStorage["asc"]);
    myValue.confirmLogin = true;
    localStorage["asc"] = JSON.stringify(myValue);
    dispatch({
      type: "CONFIRM_LOGIN",
    });
  };

  const soundplay= () => {
    audio.play();
  }

  if( waktu >= 8 ) {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/agenda" />
      </motion.Fragment>
    )
  }

  if (width > 1366 && firstLogin === "true" && login === true && confirmLogin) {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/tutorial" />
      </motion.Fragment>
    );
  }

  if (
    width <= 1366 &&
    firstLogin === "true" &&
    login === true &&
    confirmLogin
  ) {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/checkOrientation" />
      </motion.Fragment>
    );
  }

  if (confirmLogin && agenda === "false") {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/agenda" />
      </motion.Fragment>
    );
  }

  if (confirmLogin && agenda) {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/spaceship" />
      </motion.Fragment>
    );
  }

  return (
    <>
      <div className="main-background ">
        <div className="header__logo">
          <div className="logo1"></div>
          <div className="logo1"></div>
        </div>
      </div>
      <div className="centered">
        <AwesomeButton
          onPress={() => {setLogin(); soundplay()}}
          style={{
            width: "319px",
            height: "57px",
            fontWeight: "400",
            fontSize: "24px",
          }}
        >
          MASUK
        </AwesomeButton>
      </div>
    </>
  );
}

export default Welcome;
