import React, { useState, useEffect } from "react";
import axios from "axios";
import { APIURL } from "../helper/Api";
import { useSelector, useDispatch } from "react-redux";
import { motion } from "framer-motion";

export default function Tampilan() {
  const [userData, setuserData] = useState([]);
  const token = useSelector((state) => state.token);

  useEffect(() => {
    if(token) {
      let intervalComment = setInterval(() => getUserComment(), 5000);

      return () => {
        clearInterval(intervalComment);
      }
    }
  }, []);

  const getUserComment = () => {
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    axios
      .get(`${APIURL}/api/get-all-user`, config)
      .then((res) => {
        setuserData(res.data.success);
        // console.log(res.data.success)
      })
      .catch((err) => console.log({ err }));
  }

  const renderSlider = () => {
    if (userData.length) {
      return userData.map((employee) => {
        // console.log(employee.nik)
        if (
          employee.nik === "finley" ||
          employee.nik === "dimas" ||
          employee.nik === "andika" ||
          employee.nik === "eunike" ||
          employee.nik === "eza" ||
          employee.nik === "maggy" ||
          employee.nik === "deo"
        ) {
          return null;
        } else if (employee.comment) {
          return (
            <div className="grid-item-tampilan grid-blink" key={employee.id}>
              <img
                src={`https://backend-asc2021.yokesen.com/${employee.photo}`}
                alt="commitment wall"
                className="commitment-picture Blink"
              />
            </div>
          );
        } else {
          return (
            <div
              className="grid-item-tampilan overlay-picture"
              key={employee.id}
            >
              <img
                src={`https://backend-asc2021.yokesen.com/${employee.photo}`}
                alt="commitment wall"
                className="commitment-picture commitment-gray"
              />
            </div>
          );
        }
      });
    }
  };
  return <div className="grid-container-tampilan">{renderSlider()}</div>;
}
