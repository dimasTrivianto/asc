import React, { useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { Spinner } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import { loginAction } from "../redux/actions/loginAction";
import { APIURL } from "../helper/Api";
import { AwesomeButton } from "react-awesome-button";
import { motion } from "framer-motion";
import Logo1 from "../assets/images/logo-1.png";
import Logo2 from "../assets/images/logo-2.png";
import door1 from "../assets/door_sound1.mp3";

function Login() {
  const [errClass, seterrClass] = useState(false);
  const inputRef = useRef();
  const login = useSelector((state) => state.login);
  const loading = useSelector((state) => state.loading);
  const error = useSelector((state) => state.error);
  const dispatch = useDispatch();
  const firstLogin = useSelector((state) => state.firstLogin);
  let audio = new Audio(door1);

  // console.log(`isi firstLogin ${firstLogin}, isi ${isBrowser}, isi login ${login}`)
  const cek = () => {
    let invCode = inputRef.current.value;
    // console.log(invCode);
    dispatch({ type: "LOADING" });
    axios
      .post(`${APIURL}/api/login`, { nik: invCode, mainEvent: 1 })
      .then((res) => {
        console.log(res);
        const id = res.data.success.users.id;
        const nik = res.data.success.users.nik;
        const name = res.data.success.users.name;
        const firstLogin = res.data.success.firstLogin;
        const token = res.data.success.token;
        const agenda = res.data.success.agendaCheck;
        dispatch(
          loginAction({
            id: id,
            nik: nik,
            name: name,
            firstLogin,
            token,
            login: true,
            loading: false,
            confirmLogin: false,
            agenda,
          })
        );
      })
      .catch((err) => {
        console.log(err);
        dispatch({
          type: "LOGIN_FAILED",
        });
      });
  };

  const soundplay = () => {
    audio.play();
  };

  if (error) {
    dispatch({
      type: "LOGOUT",
    });
    seterrClass(true);
  }

  if (login) {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/welcome" />
      </motion.Fragment>
    );
  }

  return (
    <div className="main-background login-page">
      {/* <div className="header__logo"> */}
      <img src={Logo1} alt="logo1" className="logo1 img-fluid" />
      <img src={Logo2} alt="logo2" className="logo2 img-fluid" />
      {/* <div className="logo1"></div>
        <div className="logo1"></div> */}
      {/* </div> */}
      <div className="centered">
        {loading === true ? ( //If
          <div>
            <h4>Mempersiapkan pengalaman terbaik untuk Anda</h4>
            <Spinner
              animation="border"
              role="status"
              style={{
                width: "100px",
                height: "100px",
                fontSize: "28px",
                color: "white",
                marginTop: "30px",
              }}
            >
              <span className="sr-only">Loading...</span>
            </Spinner>
          </div>
        ) : (
          //Else
          <div>
            <h4
              style={{
                lineHeight: "33px",
              }}
              className="mb-3"
            >
              Winning through Execution and Digital
            </h4>
            <h4 className="mb-3">for</h4>
            <h1 className="mb-3 font-italic">Double Digit (DD) Growth</h1>
            <h5 style={{ marginBottom: "60px" }}>
              Begin your experience in Pharma Annual Sales Conference 2021
            </h5>
            <p>Enter your CWID</p>
            <input
              type="text"
              className={
                errClass === true ? "invcode-input-err" : "invcode-input"
              }
              onInput={() => seterrClass(false)}
              ref={inputRef}
              placeholder="CWID"
            />
            {errClass === true ? (
              <p className="input-error">CWID yang Anda masukkan salah 
              <br />
              atau anda sudah login di device lain
              </p>
            ) : null}
            <AwesomeButton
              onPress={() => {
                cek();
                soundplay();
              }}
              style={{
                width: "227px",
                height: "56.69px",
                letterSpacing: "1px",
                fontWeight: "400",
                fontSize: "24px",
                marginTop: "10px",
              }}
            >
              MASUK
            </AwesomeButton>
          </div>
        )}
      </div>
    </div>
  );
}

export default Login;
