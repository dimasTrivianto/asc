import React, { useState, useEffect } from 'react'
import { AwesomeButton } from "react-awesome-button";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { APIURL } from "../helper/Api";
import { motion } from "framer-motion";
import { Redirect } from "react-router-dom";
import door1 from "../assets/door_sound1.mp3";
import moment from 'moment'


export default function Agenda() {
  const token = useSelector((state) => state.token);
  // const agenda = useSelector((state) => state.agenda);
  const [checkList, setcheckList] = useState(false);
  const login = useSelector((state) => state.login);
  const day = moment().format("D");
  
  let audio = new Audio(door1);
  const dispatch = useDispatch();

  useEffect(() => {
    console.log(day)
  }, []);
  const checked = () => {
    setcheckList(!checkList);
  };

  const soundplay= () => {
    audio.play();
  }

  const moving = () => {
    if (token) {
      if (checkList === true) {
        const config = {
          headers: { Authorization: `Bearer ${token}` },
        };

        axios
          .post(`${APIURL}/api/agenda`, {}, config)
          .then((res) => {
            if (res.data.success.message === "Save successfull") {
              const userData = JSON.parse(localStorage.getItem("asc"));

              userData.agenda = "true";

              localStorage.setItem("asc", JSON.stringify(userData));
              dispatch({
                type: "AGENDA",
              });
            }
          })
          .catch((err) => console.log({ err }));
      } else {
        console.log("no checklist");
      }
    } else {
      console.log("no token");
    }
  };

  // Redirect to login page if user logout or not logged in
  if (login === false || login === "") {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/" />
      </motion.Fragment>
    );
  }
  
  return (
    <div className="container-fluid full-width ">
      <div className="main-background">
        <div className="col main-display-video">
          <div className="today-agenda">
            {
              // day == '2/23/2021' ? (
              //   <h3 className="date-title-front">Day 1 - February 23, 2021</h3>
              //   ) : day == '2/24/2021' ? (
              //     <h3 className="date-title-front">Day 2 - February 24, 2021</h3>
              //   ) : 
                day == '26' ? (
                  <h3 className="date-title-front">Day 4 - February 26, 2021</h3>         
                ) : (
                  <h3 className="date-title-front">Day 3 - February 25, 2021</h3>       
                  ) 
            }
            {
              // day == '2/23/2021' ? (
              //   <div className="table-wrapper">
              //     <table className="table table-borderless">
              //       <tbody>
              //         <tr className="table-color">
              //           <td>Participants Joining</td>
              //           <td>08.30 - 08.45</td>
              //         </tr>
              //         <tr className="table-color">
              //           <td>Opening</td>
              //           <td>08.45 - 08.55</td>
              //         </tr>
              //         <tr className="table-color">
              //           <td>BusinessUpdates</td>
              //           <td>08.55 - 10.45</td>
              //         </tr>
              //         <tr className="table-color">
              //           <td>Q&A Session</td>
              //           <td>10.45 - 11.00</td>
              //         </tr>
              //         <tr className="table-color">
              //           <td>Video Competition</td>
              //           <td>11.00 - 11.30</td>
              //         </tr>
              //         <tr className="table-color">
              //           <td>Awarding Session</td>
              //           <td>11.30 - 12.00</td>
              //         </tr>
              //         <tr className="table-color">
              //           <td>Prized Games</td>
              //           <td>12.00 - 12.20</td>
              //         </tr>
              //         <tr className="table-color">
              //           <td>Awarding Session</td>
              //           <td>12.20 - 12.40</td>
              //         </tr>
              //         <tr className="table-color">
              //           <td>Prized Games</td>
              //           <td>12.40 - 13.00</td>
              //         </tr>
              //         <tr className="table-color">
              //           <td>Closing</td>
              //           <td>13.00 - 13.30</td>
              //         </tr>
              //       </tbody>
              //     </table>
              //   </div>
              // ) : day == '2/24/2021' ? (
              //   <div className="table-wrapper">
              //     <table className="table table-borderless">
              //       <tbody>
              //         <tr className="table-color table-center">
              //           <td>General Medicine</td>
              //           <td>08.40-15.00</td>
              //         </tr>
              //         <tr className="table-color table-center">
              //           <td>Women's HealthCare</td>
              //           <td>08:40-15.30</td>
              //         </tr>
              //         <tr className="table-color table-center">
              //           <td>Special Medicine OPTHA</td>
              //           <td>08:40-15:00</td>
              //         </tr>
              //         <tr className="table-color table-center">
              //           <td>Special Medicine ONCO</td>
              //           <td>08:40-15:00</td>
              //         </tr>
              //         <tr className="table-color table-center">
              //           <td>Business Channel</td>
              //           <td>08:40-15:00</td>
              //         </tr>
              //         <tr className="table-color table-center">
              //           <td>Radiology</td>
              //           <td>08:40-15:30</td>
              //         </tr>
              //       </tbody>
              //     </table>
              //   </div>
              // ) : 
              
                
                <div className="table-wrapper">
                <table className="table table-borderless">
                  <tbody>
                    <tr className="table-color table-center">
                      <td>Cross Functional Presentation</td>
                      <td>09:00-15:00</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              
              }
                  
            <motion.Fragment exit="undefined">
              <Link to="/spaceship">
                <AwesomeButton className="agenda-ok" onPress={() => {moving(); soundplay()}}>
                  OK
                </AwesomeButton>
              </Link>
            </motion.Fragment>
            <div className="form-group form-check agenda-check">
              <input
                type="checkbox"
                className="form-check-input"
                id="showAgenda"
                onChange={checked}
              />
              <label className="form-check-label text-white">
                Jangan tampilkan lagi
              </label>
            </div>
          </div>
          {/* <div className="blanket"></div> */}
        </div>
      </div>
    </div>
  );
}
