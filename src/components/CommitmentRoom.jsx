import React, { useState, useRef, useEffect } from "react";
import Ellipse from "../assets/images/Ellipse_blue.svg";
import axios from "axios";
import { AwesomeButton } from "react-awesome-button";
import { APIURL } from "../helper/Api";
import { useSelector, useDispatch } from "react-redux";
import x from "../assets/images/x.png";
import { Redirect, Link } from "react-router-dom";
import Slider from "react-slick";
import Back from "../assets/images/backButton.svg";
import room_label from "../assets/images/room_label.png";
import { motion } from "framer-motion";
import door1 from "../assets/door_sound1.mp3";

export default function CommitmentRoom() {
  const [popUp, setPopUp] = useState("");
  const [modal, setmodal] = useState("");
  const [room, setRoom] = useState("");
  const [textEffect, settextEffect] = useState("");
  const [wallInput, setwallInput] = useState(false);
  const [submitInput, setsubmitInput] = useState(false);
  const commitmentRef = useRef("");
  const [errorCheck, seterrorCheck] = useState("noInput");
  const login = useSelector((state) => state.login);
  // const nik = useSelector((state) => state.nik);
  // const name = useSelector((state) => state.name);
  const token = useSelector((state) => state.token);
  const id = useSelector((state) => state.id);
  const [userData, setuserData] = useState([]);
  const [comments, setcomments] = useState([]);
  const dispatch = useDispatch();
  let audio = new Audio(door1);

  const configs = {
    className: "slick-carousel-commitment",
    infinite: false,
    arrows: false,
    centerPadding: "200px",
    slidesToShow: 5,
    swipeToSlide: true,
    rows: 2,
  };

  useEffect(() => {
    if (token) {
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      axios
        .get(`${APIURL}/api/check-comment`, config)
        .then((res) => {
          if (res.data.success.comment === "true") {
            setwallInput(true);
          } else {
            setPopUp("one");
            setmodal("modal-active");
            setRoom("room4");
            settextEffect("textEffect");
          }
        })
        .catch((err) => console.log("dua"));

      getOnlyComment();
      let intervalComment = setInterval(() => getUserComment(), 5000);

      return () => {
        clearInterval(intervalComment);
      };
    }
  }, []);

  const getUserComment = () => {
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    axios
      .get(`${APIURL}/api/get-all-user`, config)
      .then((res) => {
        setuserData(res.data.success);
        console.log(res.data.success);
      })
      .catch((err) => console.log({ err }));
  };

  const getOnlyComment = () => {
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    axios.get(`${APIURL}/api/comment`, config).then((res) => {
      setcomments(res.data.success.comment);
    });
  };

  const unfold = (params) => {
    setPopUp("one");
    setmodal("modal-active");
    setRoom(params);
    settextEffect("textEffect");
    getOnlyComment();
  };

  const folding = () => {
    setPopUp("out");
    setmodal("");
    settextEffect("");
  };

  const submitCommitment = () => {
    const comment = commitmentRef.current.value;
    if (comment.length === 0) {
      seterrorCheck("haveInput");
    } else {
      setsubmitInput(true);
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      const data = {
        comment,
        user_id: id,
      };

      return axios
        .post(`${APIURL}/api/comment`, data, config)
        .then((res) => {
          // console.log(res);
        })
        .catch((err) => {
          console.log({ err });
        });
    }
  };

  const renderSlider = () => {
    if (userData.length) {
      return userData.map((employee) => {
        // console.log(employee.nik)
        if (
          employee.nik === "finley" ||
          employee.nik === "dimas" ||
          employee.nik === "andika" ||
          employee.nik === "eunike" ||
          employee.nik === "eza" ||
          employee.nik === "maggy" ||
          employee.nik === "deo"
        ) {
          return null;
        } else 
        if (employee.comment) {
          return (
            <div className="grid-item grid-blink" key={employee.id}>
              <img
                src={`https://backend-asc2021.yokesen.com/${employee.photo}`}
                alt="commitment wall"
                className="commitment-picture"
              />
            </div>
          );
        } else {
          return (
            <div className="grid-item overlay-picture" key={employee.id}>
              <img
                src={`https://backend-asc2021.yokesen.com/${employee.photo}`}
                alt="commitment wall"
                className="commitment-picture commitment-gray"
              />
            </div>
          );
        }
      });
    }
  };

  const renderNotes = () => {
    let oddNumber = 0;
    let evenNumber = 0;

    if (comments.length) {
      // console.log(comments)
      return comments.map((comment, index) => {
        if (index % 2 === 1) {
          oddNumber++;
          if (oddNumber % 2 === 1) {
            return (
              <div className="carousel-commitment" key={index}>
                <h3 className="comment-text">{comment.comment}</h3>
              </div>
            );
          } else if (oddNumber % 2 === 0) {
            return (
              <div className="carousel-commitment-2" key={index}>
                <h3 className="comment-text">{comment.comment}</h3>
              </div>
            );
          }
        } else {
          evenNumber = evenNumber + 1;
          if (evenNumber % 2 === 1) {
            return (
              <div className="carousel-commitment-2" key={index}>
                <h3 className="comment-text">{comment.comment}</h3>
              </div>
            );
          } else if (evenNumber % 2 === 0) {
            return (
              <div className="carousel-commitment" key={index}>
                <h3 className="comment-text">{comment.comment}</h3>
              </div>
            );
          }
        }
      });
    }
  };

  const soundplay = () => {
    audio.play();
  };

  const containerVariant = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: { duration: 1 },
    },
    exit: {
      opacity: 0,
      transition: { duration: 1 },
    },
  };

  //User Logout
  const logout = () => {
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    axios
      .post(`${APIURL}/api/logout`, {}, config)
      .then((res) => {
        localStorage.removeItem("asc");
        dispatch({
          type: "LOGOUT",
        });
      })
      .catch((err) => {
        console.log("gagal logout");
      });
  };

  // if (login === false || login === "") {
  //   return <Redirect to="/" />;
  // }

  // Redirect to login page if user logout or not logged in
  if (login === false || login === "") {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/" />
      </motion.Fragment>
    );
  }

  return (
    <motion.div
      variants={containerVariant}
      initial="hidden"
      animate="visible"
      exit="exit"
      className={`test-commitment ${modal}`}
    >
      {/* <div className="blanket"></div> */}
      <div className="back" onClick={soundplay}>
        <Link to="/home">
          <img src={Back} alt="back_button" className="back_button" />
        </Link>
      </div>
      <div>
        <img
          src={room_label}
          alt="conference room"
          className="room-label-img"
        />
        <h4 className="room-label">Commitment Room</h4>
      </div>
      <div className="logout-video">
          <AwesomeButton
            type="secondary"
            style={{ fontSize: "28px" }}
            onPress={() => {
              logout();
              soundplay();
            }}
          >
          <i className="fas fa-sign-out-alt"></i>
        </AwesomeButton>
      </div>
      <div
        className="wall-room"
        onClick={() => {
          unfold("room1");
          renderSlider();
          soundplay();
        }}
      >
        <div className="commitment-bold Blink"></div>
        <img
          src={Ellipse}
          className=" wall-link"
          alt="Wall Room"
          style={{ marginBottom: "15px" }}
        />
        <h5 className="blue-text-bg">Commitment wall</h5>
        {/* <p>
            Write your hopes, dreams, and commitment <br /> for 2021 right here
          </p> */}
      </div>
      <div
        className="notes-room"
        onClick={() => {
          unfold("room2");
          soundplay();
        }}
      >
        <div className="commitment-bold1 Blink"></div>
        <img
          src={Ellipse}
          className=" notes-link"
          alt="Notes Room"
          style={{ marginBottom: "15px" }}
        />
        <h5 className="blue-text-bg">Notes wall</h5>
        {/* <p>
            Write your hopes, dreams, and commitment <br /> for 2021 right here
          </p> */}
      </div>
      <div
        className="entertainment-room"
        onClick={() => {
          unfold("room3");
          soundplay();
        }}
      >
        <div className="commitment-bold2 Blink"></div>
        <img
          src={Ellipse}
          className=" entertainment-link"
          alt="entertainment Room"
          style={{ marginBottom: "15px" }}
        />
        <h5 className="blue-text-bg">Entertainment wall</h5>
        {/* <p>
            Write your hopes, dreams, and commitment <br /> for 2021 right here
          </p> */}
      </div>

      {/* commitment Room Modal  */}
      <div id="modal-container" className={`${popUp}`}>
        <div className="modal-background">
          <div className="modal">
            <div className="container-fluid full-width">
              {room === "room3" ? (
                <div className="centering-bg">
                  <div className="col main-display">
                    <img
                      src={x}
                      alt="close"
                      className="x-image"
                      onClick={() => {
                        folding();
                        soundplay();
                      }}
                    />
                    <div
                      className={`text-light text-content-zoom ${textEffect}`}
                    >
                      <p>Entertainment Room</p>
                      <AwesomeButton
                        style={{
                          width: "246px",
                          height: "56.69px",

                          fontWeight: "400",
                          fontSize: "24px",
                          marginTop: "10px",
                        }}
                        href="https://www.youtube.com/watch?v=bQ_6WRg2oCE"
                        target="_blank"
                      >
                        ENTER TO ENTERTAINMENT ROOM
                      </AwesomeButton>
                    </div>
                  </div>
                  <div className="col bottom-display">
                    <div className="wrapper-text-bottom">
                      <h1 className={`text-light title-bottom ${textEffect}`}>
                        Anda berada di Entertainment Hall
                      </h1>
                      {/* <p className={`text-light text-bottom ${textEffect}`}>
                        Bayer has prepared to do the biggest expansion! The main
                        idea is winning through execution and digital for Double
                        Digit (DD) Growth.
                      </p> */}
                      {/* <br />
                      <p
                        className={`text-light text-bottom ${textEffect}`}
                      >
                        Have you prepared yourself for the Double Digit Growth
                        strategy? Be ready and join the vibes right now!
                      </p> */}
                    </div>
                  </div>
                </div>
              ) : room === "room1" ? (
                <div className="centering-bg">
                  <div className="col main-display">
                    <img
                      src={x}
                      alt="close"
                      className="x-image"
                      onClick={() => {
                        folding();
                        soundplay();
                      }}
                    />
                    <div
                      className={`text-light text-content-commitment ${textEffect}`}
                    >
                      <div className="grid-container">{renderSlider()}</div>
                      {/* <img src={commitment} alt="commitment wall" className="commitment-picture"/> */}
                      {/* <Slider {...configs}>{renderSlider()}</Slider> */}
                    </div>
                  </div>
                  <div className="col bottom-display">
                    <div className="wrapper-text-bottom">
                    <h1 className={`text-light title-bottom ${textEffect}`}>
                        Inilah Commitment Wall kita.
                      </h1>
                      <p className={`text-light text-bottom ${textEffect}`}>
                        Suatu sore di kota Palu, ada kunjungan dengan Dokter Paru
                        <br />
                        Tahun 2020 sudah berlalu, saatnya kita memulai lembaran baru
                        <br />
                        <br />
                        Mari kita lihat berbagai komitmen rekan kita untuk lembaran baru di tahun ini!
                      </p>
                    </div>
                  </div>
                </div>
              ) : room === "room2" ? (
                <div className="centering-bg">
                  <div className="col main-display">
                    <img
                      src={x}
                      alt="close"
                      className="x-image"
                      onClick={() => {
                        folding();
                        soundplay();
                      }}
                    />
                    <div
                      className={`text-light text-content-zoom ${textEffect}`}
                    >
                      <Slider {...configs}>{renderNotes()}</Slider>
                    </div>
                  </div>
                  <div className="col bottom-display">
                    <div className="wrapper-text-bottom">
                      <h1 className={`text-light title-bottom ${textEffect}`}>
                        Inilah Commitment Wall kita.
                      </h1>
                      <p className={`text-light text-bottom ${textEffect}`}>
                        Suatu sore di kota Palu, ada kunjungan dengan Dokter Paru
                        <br />
                        Tahun 2020 sudah berlalu, saatnya kita memulai lembaran baru
                        <br />
                        <br />
                        Ayo! Tuliskan komitmen kita untuk mencapai kesuksesan tahun ini!
                      </p>
                    </div>
                  </div>
                </div>
              ) : room == "room4" ? (
                <div className="centering-bg">
                  <div className="col main-display">
                    <img
                      src={x}
                      alt="close"
                      className="x-image"
                      onClick={() => {
                        folding();
                        soundplay();
                      }}
                    />
                    {submitInput ? (
                      <div
                        className={`text-light text-content-zoom ${textEffect}`}
                      >
                        <h5>Terima kasih, komitmen Anda sudah diterima Commander!</h5>
                        <p>Lihat juga komitmen rekan Anda di Commitment Gallery.</p>
                        <AwesomeButton
                          className={`text-light btn meeting-btn ${textEffect}`}
                          onPress={() => {
                            folding();
                            soundplay();
                          }}
                        >
                          CONTINUE
                        </AwesomeButton>
                      </div>
                    ) : (
                          <div
                            className={`text-light text-content-zoom ${textEffect}`}
                          >
                            <h5>Ayo! Tuliskan komitmen kita untuk mencapai kesuksesan tahun ini!</h5>
                              <p className={`${errorCheck} text-danger`}>
                                Please fill your commitment
                              </p>
                              <form>
                                <div className="form-group">
                                  <textarea maxLength="300"
                                  ref={commitmentRef}
                                  className="form-control black-input"
                                  id="wallInput"
                                  rows="3"
                                  placeholder="Textbox"
                                ></textarea>
                                {/* <p id="text-counter">200/300</p> */}
                              </div>
                            </form>
                            <AwesomeButton
                              className={`text-light btn meeting-btn ${textEffect}`}
                              onPress={() => {submitCommitment(); soundplay()}}
                            >
                              SUBMIT
                            </AwesomeButton>
                          </div>
                    )}
                  </div>
                  <div className="col bottom-display">
                    <div className="wrapper-text-bottom">
                      <h1 className={`text-light title-bottom ${textEffect}`}>
                        Inilah Commitment Wall kita.
                      </h1>
                      <p className={`text-light text-bottom ${textEffect}`}>
                        Suatu sore di kota Palu, ada kunjungan dengan Dokter Paru
                        <br />
                        Tahun 2020 sudah berlalu, saatnya kita memulai lembaran baru
                        <br />
                        <br />
                        Ayo! Tuliskan komitmen kita untuk mencapai kesuksesan tahun ini!
                      </p>
                    </div>
                  </div>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
}
