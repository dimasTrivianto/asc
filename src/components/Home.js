import React, { useState, useRef, useEffect } from "react";
import Ellipse from "../assets/images/Ellipse_blue.svg";
import x from "../assets/images/x.png";
import titleBg from "../assets/images/title-bg.png";
import axios from "axios";
import { APIURL } from "../helper/Api";
import { useSelector, useDispatch } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import Slider from "react-slick";
// import { firstLogin } from "../redux/actions/loginAction";
// import vid from "../assets/vid.mp4";
import { AwesomeButton } from "react-awesome-button";
import { loginAction } from "../redux/actions/loginAction";
import Carousel from "./carousel";
import { motion } from "framer-motion";
import door1 from "../assets/door_sound1.mp3";
import door2 from "../assets/door_sound2.mp3";
import door3 from "../assets/door_sound3.mp3";
import CommitmentRoom from "./CommitmentRoom";

function Homepage() {
  const [popUp, setPopUp] = useState("");
  const [modal, setmodal] = useState("");
  const [room, setRoom] = useState("");
  const [textEffect, settextEffect] = useState("");
  const [wallInput, setwallInput] = useState(false);
  const [submitInput, setsubmitInput] = useState(false);
  const commitmentRef = useRef("");
  const [errorCheck, seterrorCheck] = useState("noInput");
  const login = useSelector((state) => state.login);
  const nik = useSelector((state) => state.nik);
  const name = useSelector((state) => state.name);
  const token = useSelector((state) => state.token);
  const id = useSelector((state) => state.id);
  // const [comments, setcomments] = useState([]);
  // const [countLike, setcountLike] = useState(null)
  const [videos, setvideos] = useState([]);
  const [userData, setuserData] = useState([]);
  const tanggal = new Date().toLocaleDateString();
  const waktu = +new Date().getHours();
  const menit = +new Date().getMinutes();
  // const slideRef = useRef();
  const agenda = useSelector((state) => state.agenda);
  let audio1 = new Audio(door1);
  let audio2 = new Audio(door2);
  let audio3 = new Audio(door3);

  // console.log(agenda);
  // console.log(menit);
  // console.log(name);
  const dispatch = useDispatch();
  const settings = {
    className: "slick-carousel",
    infinite: false,
    arrows: false,
    centerPadding: "200px",
    slidesToShow: 4,
    swipeToSlide: true,
  };

  const configs = {
    className: "slick-carousel-commitment",
    infinite: false,
    arrows: false,
    centerPadding: "200px",
    slidesToShow: 5,
    swipeToSlide: true,
    rows: 2,
  };

  const setting = {
    dragSpeed: 1.25,
    itemWidth: 313,
    itemHeight: 450,
    itemSideOffsets: 0,
  };

  const itemStyle = {
    width: `${setting.itemWidth}px`,
    height: `${setting.itemHeight}px`,
    margin: `0px ${setting.itemSideOffsets}px`,
  };

  useEffect(() => {
    console.log(tanggal);
    if (token) {
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      axios
        .get(`${APIURL}/api/check-comment`, config)
        .then((res) => {
          if (res.data.success.comment === "true") {
            setwallInput(true);
          }
        })
        .catch((err) => console.log({ err }));

      getUserComment();
      // getVideo();
      // console.log(videos)
      // if(firstLogin === 'true'){
      //   dispatch(firstLogin({firstLogin: 'false'}))
      // }
      //console.log(new Date().toDateString())
    }
  }, []);

  const soundPlay = (room) => {
    if (room === "commitmentRoom") {
      // audio1.volume = 0.7
      audio1.play();
    }
    if (room === "videoRoom") audio2.play();
    if (room === "conferenceRoom") audio3.play();
  };
  // const getVideo = () => {
  //   const config = {
  //     headers: { Authorization: `Bearer ${token}` },
  //   };
  //   axios
  //     .get(`${APIURL}/api/video`, config)
  //     .then((res) => {
  //       setvideos(res.data.success.video);
  //       // console.log(res.data.success.video)
  //     })
  //     .catch((err) => console.log({ err }));
  // };

  const getUserComment = () => {
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    axios
      .get(`${APIURL}/api/get-all-user`, config)
      .then((res) => {
        console.log(res.data.success);
        // setcomments(res.data.success.comment);
        setuserData(res.data.success);
      })
      .catch((err) => console.log({ err }));
  };
  const unfold = (params) => {
    setPopUp("one");
    setmodal("modal-active");
    setRoom(params);
    settextEffect("textEffect");
  };

  const folding = () => {
    setPopUp("out");
    setmodal("");
    settextEffect("");
  };

  const submitCommitment = () => {
    const comment = commitmentRef.current.value;
    if (comment.length === 0) {
      seterrorCheck("haveInput");
    } else {
      setsubmitInput(true);
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      const data = {
        comment,
        user_id: id,
      };

      return axios
        .post(`${APIURL}/api/comment`, data, config)
        .then((res) => {
          // console.log(res);
        })
        .catch((err) => {
          console.log({ err });
        });
    }
  };

  //User Logout
  const logout = () => {
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    axios
      .post(`${APIURL}/api/logout`, {}, config)
      .then((res) => {
        localStorage.removeItem("asc");
        dispatch({
          type: "LOGOUT",
        });
      })
      .catch((err) => {
        console.log("gagal logout");
      });
  };

  const seeCommitmentWall = () => {
    setwallInput(true);
    getUserComment();
  };

  // const count = (video_id, nik) => {
  //   const config = {
  //     headers: { Authorization: `Bearer ${token}` },
  //   };
  //   const data = {
  //     user_id: nik,
  //     video_id: video_id,
  //   };
  //   axios
  //     .post(`${APIURL}/api/like`, data, config)
  //     .then((res) => {
  //       // console.log(res.data)
  //       // getVideo();
  //     })
  //     .catch((err) => {
  //       console.log({ err });
  //     });
  // };

  const renderSlider = () => {
    if (userData.length) {
      return userData.map((employee) => {
        // console.log(`isi ${employee.photo}`)
        if (employee.comment) {
          return (
            <div className="grid-item grid-blink">
              <img
                src={`https://backend-asc2021.yokesen.com/${employee.photo}`}
                alt="commitment wall"
                className="commitment-picture Blink"
              />
            </div>
          );
        } else {
          return (
            <div className="grid-item overlay-picture">
              <img
                src={`https://backend-asc2021.yokesen.com/${employee.photo}`}
                alt="commitment wall"
                className="commitment-picture commitment-gray"
              />
            </div>
          );
        }
      });
    }
  };

  // const renderVideo = () => {
  //   if (videos.length) {
  //     return videos.map((vid) => {
  //       // console.log(vid)
  //       return (
  //         <div className="slider-wrapper" key={vid.id}>
  //           <video type="video/mp4" controls>
  //             <source src={"http://backend-asc2021.yokesen.com/" + vid.url} />
  //           </video>
  //           {vid.userLike === 1 ? (
  //             <button
  //               className="like-button"
  //               onClick={() => count(vid.id, vid.nik)}
  //             >
  //               <i className="fas fa-heart"></i>
  //               <span className="counter">{vid.totalLike}</span>
  //             </button>
  //           ) : (
  //             <button
  //               className="like-button"
  //               onClick={() => count(vid.id, vid.nik)}
  //             >
  //               <i className="far fa-heart"></i>
  //               <span className="counter">{vid.totalLike}</span>
  //             </button>
  //           )}
  //         </div>
  //       );
  //     });
  //   }
  // };

  // Redirect to login page if user logout or not logged in
  if (login === false || login === "") {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/" />
      </motion.Fragment>
    );
  }

  const containerVariant = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: { duration: 1 },
    },
    exit: {
      opacity: 0,
      transition: { duration: 1 },
    },
  };

  return (
    <motion.div
      variants={containerVariant}
      initial="hidden"
      animate="visible"
      exit="exit"
      // initial={{ opacity: 0 }}
      // animate={{ opacity: 1 }}
      // transition={{ duration: 3 }}
      // exit={{ opacity: 0 }}
      className={` ${modal}`}
    >
      <div className="stars"></div>
      <div className="twinkling">
        <div className="home-background">
          <div className="header__title">
            <img src={titleBg} alt="" className="title-bg-home" />
            <div className="home-title">
              {/* nama dibikin scroll */}
              <h3 className="home-title-text">
                Welcome aboard{" "}
                {name.length > 10 ? name.substr(0, name.indexOf(" ")) : name},
              </h3>
              <h2 className="home-title-text">in Pharma ASC 2021 Spaceship</h2>
            </div>
          </div>
          <div className="logout">
            <AwesomeButton
              type="secondary"
              style={{ fontSize: "28px" }}
              onPress={() => {
                logout();
                soundPlay("commitmentRoom");
              }}
            >
              <i className="fas fa-sign-out-alt"></i>
            </AwesomeButton>
          </div>

          <Link to="/commitmentroom">
            <div className="room-1" onClick={() => soundPlay("commitmentRoom")}>
              <div className="button-bold1 Blink"></div>
              <img
                src={Ellipse}
                // className="Blink"
                alt=""
                style={{ marginBottom: "15px" }}
                // onClick={() => {unfold("room1"); renderSlider()}}
              />
              <h5 className="blue-text-bg">Commitment Room</h5>
            </div>
          </Link>

            <Link to="/videoroom">
              <div className="room-2" onClick={() => soundPlay("videoRoom")}>
                <div className="button-bold2 Blink"></div>
                <img
                  src={Ellipse}
                  // className="Blink"
                  alt=""
                  style={{ marginBottom: "15px" }}
                  // onClick={() => unfold("room2")}
                />
                <h5 className="blue-text-bg">Video Competition</h5>
              </div>
            </Link>

          <Link to="/conference">
            <div className="room-3" onClick={() => soundPlay("conferenceRoom")}>
              <div className="button-bold1 Blink"></div>
              <img
                src={Ellipse}
                // className="Blink"
                alt=""
                style={{ marginBottom: "15px" }}
                // onClick={() => unfold("room3")}
              />
              <h5 className="blue-text-bg">Conference Hall</h5>
            </div>
          </Link>

          <div
            className="room-4"
            onClick={() => {
              unfold("room2");
              soundPlay("commitmentRoom");
            }}
          >
            <div className="button-bold-video Blink"></div>
            <img
              src={Ellipse}
              // className="Blink"
              alt=""
              style={{ marginBottom: "15px" }}
              // onClick={() => {unfold("room2")}}
            />
            <h5 className="blue-text-bg">Video From Commander</h5>
          </div>

          <div className="nik">
            <p>CWID: {nik}</p>
          </div>

          {/* Meeting Room Modal  */}
          <div id="modal-container" className={`${popUp}`}>
            <div className="modal-background">
              <div className="modal">
                <div className="container-fluid full-width">
                  {room === "room3" ? (
                    <div className="centering-bg">
                      <div className="col main-display">
                        <img
                          src={x}
                          alt="close"
                          className="x-image"
                          onClick={folding}
                        />
                        <div
                          className={`text-light text-content-zoom ${textEffect}`}
                        >
                          <p>
                            You can't see the Competition now, please check
                            againt at February 23 2021, 09.30am
                          </p>
                        </div>
                      </div>
                    </div>
                  ) : room === "room1" ? (
                    <div className="centering-bg">
                      <div className="col main-display">
                        <img
                          src={x}
                          alt="close"
                          className="x-image"
                          onClick={folding}
                        />
                        {wallInput ? (
                          <div
                            className={`text-light text-content-commitment ${textEffect}`}
                          >
                            <div className="grid-container">
                              {renderSlider()}
                            </div>
                            {/* <img src={commitment} alt="commitment wall" className="commitment-picture"/> */}
                            {/* <Slider {...configs}>{renderSlider()}</Slider> */}
                          </div>
                        ) : submitInput ? (
                          <div
                            className={`text-light text-content-zoom ${textEffect}`}
                          >
                            <h5>Your commitment has been posted.</h5>
                            <AwesomeButton
                              className={`text-light btn meeting-btn ${textEffect}`}
                              onPress={seeCommitmentWall}
                            >
                              CONTINUE
                            </AwesomeButton>
                          </div>
                        ) : (
                          <div
                            className={`text-light text-content-zoom ${textEffect}`}
                          >
                            <h5>Your Commitment:</h5>
                            <p className={`${errorCheck} text-danger`}>
                              Please fill your commitment
                            </p>
                            <form>
                              <div className="form-group">
                                <textarea
                                  ref={commitmentRef}
                                  className="form-control black-input"
                                  id="wallInput"
                                  rows="3"
                                  placeholder="Write here..."
                                ></textarea>
                              </div>
                            </form>
                            <AwesomeButton
                              className={`text-light btn meeting-btn ${textEffect}`}
                              onPress={submitCommitment}
                            >
                              ENTER YOUR COMMITMENT
                            </AwesomeButton>
                          </div>
                        )}
                      </div>
                      <div className="col bottom-display">
                        <div className="wrapper-text-bottom">
                          <h1
                            className={`text-light title-bottom ${textEffect}`}
                          >
                            You are in Commitment Wall
                          </h1>
                          <p className={`text-light text-bottom ${textEffect}`}>
                            This year has been so hard for all of us. Beside
                            that, there’s one thing called “commitment” that can
                            bring us to moving forward so we can create
                            innovation and creative solutions. Write your hopes,
                            dreams, and commitment for 2021 right here
                          </p>
                        </div>
                      </div>
                    </div>
                  ) : room === "room2" ? (
                    <div className="centering-bg">
                      <div className="col main-display">
                        <img
                          src={x}
                          alt="close"
                          className="x-image"
                          onClick={folding}
                        />
                        <div className="video-frame-home">
                          <video
                            type="video/mp4"
                            controls
                            style={{ height: "100%", maxWidth: "100%" }}
                            // onEnded={() => move()}
                          >
                            <source src="https://backend-asc2021.yokesen.com/storage/videoReferences/Video3.mp4" />
                          </video>
                        </div>
                      </div>
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* === */}
        {/* </div> */}
        {/* ==== */}
      </div>
    </motion.div>
  );
}

export default Homepage;
