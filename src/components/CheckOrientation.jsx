import React, { useState, useEffect } from "react";
import rotate from "../assets/images/rotate your phone.gif";
import { AwesomeButton } from "react-awesome-button";
import { Link, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import { motion } from "framer-motion";

export default function CheckOrientation() {
  const [landscape, setlandscape] = useState("");
  const firstLogin = useSelector((state) => state.firstLogin);
  const login = useSelector((state) => state.login);

  useEffect(() => {
    if (window.matchMedia("(orientation: landscape)").matches) {
      // console.log(window.matchMedia("(orientation: landscape)").matches)
      setlandscape(true);
    } else if (window.matchMedia("(orientation: portrait)").matches) {
      // console.log(window.matchMedia("(orientation: portrait)").matches)
      setlandscape(false);
    }
  }, []);

  window.addEventListener("resize", function (e) {
    if (window.matchMedia("(orientation: landscape)").matches) {
      // console.log(window.matchMedia("(orientation: landscape)").matches)
      setlandscape(true);
    } else if (window.matchMedia("(orientation: portrait)").matches) {
      // console.log(window.matchMedia("(orientation: portrait)").matches)
      setlandscape(false);
    }
  });

  // Redirect to login page if user logout or not logged in
  if (login === false || login === "") {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/" />
      </motion.Fragment>
    );
  }

  // coba pakai if
  return landscape ? (
    <div className="container-fluid full-width ">
      <div className="main-background">
        <div className="col main-display-rotate">
          <div className="portrait-wrapper">
            <div className="rotate-img">
              <img src={rotate} alt="Please rotate the screen" />
            </div>
            <p className="text-rotate">
              Mohon mengganti orientasi menjadi landscape untuk memaksimalkan
              pengalaman
              {landscape}
            </p>
          </div>
          {/* <div className="blanket"></div> */}
        </div>
        {firstLogin === "true" ? (
          <motion.Fragment exit="undefined">
            <Link to="/tutorial">
              <AwesomeButton
                style={{
                  width: "30vw",
                  height: "5vh",
                  zIndex: "12",
                  letterSpacing: "1px",
                  fontWeight: "400",
                  fontSize: "24px",
                  marginRight: "20px",
                  position: "absolute",
                  top: "87%",
                  left: "36%",
                }}
              >
                MASUK
              </AwesomeButton>
            </Link>
          </motion.Fragment>
        ) : (
          <motion.Fragment exit="undefined">
            <Link to="/home">
              <AwesomeButton
                style={{
                  width: "227px",
                  height: "56.69px",
                  letterSpacing: "1px",
                  fontWeight: "400",
                  fontSize: "24px",
                  marginRight: "20px",
                  position: "absolute",
                  top: "77%",
                  left: "31.5%",
                }}
              >
                MASUK
              </AwesomeButton>
            </Link>
          </motion.Fragment>
        )}
      </div>
    </div>
  ) : (
    <div className="container-fluid full-width ">
      <div className="main-background">
        <div className="col main-display-portrait">
          <div className="landscape-wrapper">
            <div className="rotate-img-landscape">
              <img src={rotate} alt="Please rotate the screen" />
            </div>
            <p className="text-rotate-landscape">
              Mohon mengganti orientasi menjadi landscape untuk memaksimalkan
              pengalaman
              {landscape}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
