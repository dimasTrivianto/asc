import React, {useState} from 'react'
import x from "../assets/images/x.png"
// import commitmentWall from '../assets/images/commander-wall.png'

export default function CommitmentWall() {
  const [popUp, setPopUp] = useState('')
  const [modal, setmodal] = useState('')
  const unfold = () => {
    setPopUp('one')
    setmodal('modal-active')
  }

  const folding = () => {
    setPopUp('out')
    setmodal('')
  }
  return (
    <div className={`container-fluid wrapper main-background ${modal}`}>
        <div id="modal-container" className={`${popUp}`} >
          <div className="modal-background">
            <div className="modal">
            <div className="container-fluid full-width">
              <div className="centering-bg">
                <div className="col main-display">
                  <img src={x} alt="close" className="x-image" onClick={folding}/>
                  {/* <img src={commitmentWall} alt="commitment wall" class="commitment-picture"/> */}
                </div>
                <div className="col bottom-display">
                  <h1 className="text-light title-bottom">You are in Commitment Wall</h1>
                  <div className="text-light text-bottom">
                    <p>You are now a Space Warrior. Get ready and write your commitment in the wall post. The Commander is waiting for you.</p>
                  </div>
                </div>
              <div className="blanket"></div>
              </div>
            </div>
              {/* <svg className="modal-svg" xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none">
                <rect x="0" y="0" fill="none" width="226" height="162" rx="3" ry="3"></rect>
              </svg> */}
            </div>
          </div>
        </div>
          <div className="buttons">
            <div id="one" className="button" onClick={unfold}>Unfolding</div>
          </div>
    </div>
  )
}
