import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import { motion } from "framer-motion";

export default function VideoFromCommander() {
  const [moving, setmoving] = useState(false);
  const firstLogin = useSelector((state) => state.firstLogin);
  const login = useSelector((state) => state.login);
    const token = useSelector((state) => state.token);



  useEffect(() => {
    if (firstLogin === "false") {
      setmoving(true);
    }
  }, []);
  const move = () => {
    setmoving(true);
  };

  // Redirect to login page if user logout or not logged in
  if (login === false || login === "") {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/" />
      </motion.Fragment>
    );
  }

  return moving ? (
    <motion.Fragment exit="undefined">
      <Redirect to="/agenda" />
    </motion.Fragment>
  ) : (
    <div className="container-fluid full-width ">
      <div className="main-background">
        <div className="col main-display-video">
          <div className="video-frame">
            <video
              type="video/mp4"
              controls
              style={{ height: "100%", maxWidth: "100%" }}
              onEnded={() => move()}
            >
              <source src="https://backend-asc2021.yokesen.com/storage/videoReferences/Video3.mp4" />
            </video>
          </div>
        </div>
        {/* <div className="blanket"></div> */}
      </div>
    </div>
  );
}
