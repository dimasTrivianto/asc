import React, { useState } from "react";
import ellipse from "../assets/images/Ellipse_blue.svg";
import { AwesomeButton } from "react-awesome-button";
import x from "../assets/images/x.png";
import room_label from "../assets/images/room_label.png";
import button from "../assets/images/bg_button.png";
import Back from "../assets/images/backButton.svg";
import { Link, Redirect } from "react-router-dom";
import Slider from "react-slick";
import { motion } from "framer-motion";
import { useSelector, useDispatch } from "react-redux";
import door1 from "../assets/door_sound1.mp3";
import agenda from "../assets/files/agenda.pdf";
import axios from "axios";
import { APIURL } from "../helper/Api";
import moment from 'moment'

function Conference() {
  const [popUp, setPopUp] = useState("");
  const [textEffect, settextEffect] = useState("");
  const [modal, setmodal] = useState("");
  const [room, setRoom] = useState("");
  const [roomName, setRoomName] = useState("");
  const tanggal1 = new Date().toLocaleDateString();
  const waktu1 = +new Date().getHours();
  const tanggal =  moment().format("D");
  // const tanggal2 = new Date().toLocaleDateString();
  const waktu2 = +moment().format("H");
  // const waktu5 = +moment().format("HH:mm");
  const waktu6 = +moment().format("m");
  const tanggal3 = new Date().toLocaleDateString();
  const waktu3 = +new Date().getHours();
  const tanggal4 = new Date().toLocaleDateString();
  const waktu4 = +new Date().getHours();
  const login = useSelector((state) => state.login);
  const token = useSelector((state) => state.token);
  const dispatch = useDispatch();

  console.log(`tanggal = ${typeof(tanggal)}`)

  let audio = new Audio(door1);

  const containerVariant = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: { duration: 1 },
    },
    exit: {
      opacity: 0,
      transition: { duration: 1 },
    },
  };

  const configs = {
    className: "slick-carousel-agenda",
    infinite: false,
    arrows: false,
    dots: true,
    centerPadding: "200px",
    slidesToShow: 1,
    swipeToSlide: true,
  };

  const soundplay = () => {
    audio.play();
  };

  const unfold = (params, name) => {
    setPopUp("one");
    setmodal("modal-active");
    setRoom(params);
    settextEffect("textEffect");
    setRoomName(name);
  };

  const folding = () => {
    setPopUp("out");
    setmodal("");
    settextEffect("");
  };

  //User Logout
  const logout = () => {
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    axios
      .post(`${APIURL}/api/logout`, {}, config)
      .then((res) => {
        localStorage.removeItem("asc");
        dispatch({
          type: "LOGOUT",
        });
      })
      .catch((err) => {
        console.log("gagal logout");
      });
  };

  // Redirect to login page if user logout or not logged in
  if (login === false || login === "") {
    return (
      <motion.Fragment exit="undefined">
        <Redirect to="/" />
      </motion.Fragment>
    );
  }

  return (
    <motion.div
      variants={containerVariant}
      initial="hidden"
      animate="visible"
      exit="exit"
      className="conference-bg"
    >
      <div className="back" onClick={soundplay}>
        <Link to="/home">
          <img src={Back} alt="back_button" className="back_button" />
        </Link>
      </div>
      <div>
        <img
          src={room_label}
          alt="conference room"
          className="room-label-img"
        />
        <h3 className="room-label">Conference Hall</h3>
      </div>
      <div>
      </div>
      <div onClick={() => {unfold("room3", "Agenda Room"); soundplay()}}>
        <img
          src={button}
          className="agenda-button"
          
        />
          <p className="agenda-text">AGENDA</p>
        
      </div>
      <div>
        <img src={button} className="survey-button" />
        <a
          href="https://app.sli.do/event/jckxhkpp/embed/polls/56bb0bf4-7449-4563-963b-aebd2bc8c159"
          target="_blank"
          className="survey-text"
          onClick={soundplay}
        >
          SURVEY
        </a>
      </div>
      <div className="logout-conference">
          <AwesomeButton
            type="secondary"
            style={{ fontSize: "28px" }}
            onPress={() => {
              logout();
              soundplay();
            }}
          >
          <i className="fas fa-sign-out-alt"></i>
        </AwesomeButton>
      </div>
      {
        // tanggal1 == "2/26/2021" ? (
        //   <div className="conference-room-1" onClick={() => {unfold("room1", "Cross-Functional Session"); soundplay()}}>
        //   <div className="conference-bold-cross Blink"></div>
        //     <img
        //       src={ellipse}
        //       // className="Blink"
        //       alt=""
        //       style={{ marginBottom: "15px", width: "100px", height: "100px"}}
        //     />
        //     <p className="blue-text-bg">Cross-Functional Presentation</p>
        //   </div>
        // ) : tanggal1 == "2/23/2021" ? (
        
        //   <div className="conference-room-1" onClick={() => {unfold("room1", "Plenary Session"); soundplay()}}>
        //   <div className="conference-bold Blink"></div>
        //     <img
        //       src={ellipse}
        //       // className="Blink"
        //       alt=""
        //       style={{ marginBottom: "15px", width: "100px", height: "100px"}}
        //     />
        //     <p className="blue-text-bg">Plenary Session</p>
        //   </div>
        
        // ) : 
        // tanggal1 == "26" ? (
          <div className="conference-room-1" onClick={() => {unfold("room1", "Cross-Functional Session"); soundplay()}}>
          <div className="conference-bold-cross Blink"></div>
            <img
              src={ellipse}
              // className="Blink"
              alt=""
              style={{ marginBottom: "15px", width: "100px", height: "100px"}}
            />
            <p className="blue-text-bg">Cross-Functional Presentation</p>
          </div>
        // ) : (
        //   <div className="conference-room-1" onClick={() => {unfold("room2", "Cross-Functional Session"); soundplay()}}>
        //   <div className="conference-bold-cross Blink"></div>
        //     <img
        //       src={ellipse}
        //       // className="Blink"
        //       alt=""
        //       style={{ marginBottom: "15px", width: "100px", height: "100px"}}
        //     />
        //     <p className="blue-text-bg">Cross-Functional Presentation</p>
        //   </div>
        // )
      }
      
      {/* =========================================== */}
      {
        // (tanggal1 === "2/24/2021" || tanggal1 === "2/25/2021") && waktu1 >= 8 ? (
          <div className="conference-room-2" onClick={() => {unfold("room1", "Radiology"); soundplay()}}>
            <div className="conference-bold2 Blink"></div>
            <img
              src={ellipse}
              // className="Blink"
              alt=""
              style={{ marginBottom: "15px" }}
            />
            <p className="blue-text-bg">Radiology</p>
          </div>
        // ) : tanggal1 === "2/26/2021" ? (
        //   null
        // ) : (
        //   <div className="conference-room-2" onClick={() => {unfold("room2", "Radiology"); soundplay()}}>
        //     <div className="conference-bold2 Blink"></div>
        //     <img
        //       src={ellipse}
        //       // className="Blink"
        //       alt=""
        //       style={{ marginBottom: "15px" }}
        //     />
        //     <p className="blue-text-bg">Radiology</p>
        //   </div>
        // )
      }
      {/* =========================================== */}

      {/* =========================================== */}
      { 
      // (tanggal3 === "2/24/2021" || tanggal3 === "2/25/2021") && waktu3 >= 8 ? (
          <div className="conference-room-4" onClick={() => {unfold("room1", "Business Channel"); soundplay()}}>
            <div className="conference-bold3 Blink"></div>
            <img
              src={ellipse}
              // className="Blink"
              alt=""
              style={{ marginBottom: "15px" }}
              
            />
            <p className="blue-text-bg">Business Channel</p>
          </div>
        // ) : tanggal3 === "2/26/2021" ? (
        //   null
        // ) : (
        //   <div className="conference-room-4" onClick={() => {unfold("room2", "Business Channel"); soundplay()}}>
        //     <div className="conference-bold3 Blink"></div>
        //     <img
        //       src={ellipse}
        //       // className="Blink"
        //       alt=""
        //       style={{ marginBottom: "15px" }}
              
        //     />
        //     <p className="blue-text-bg">Business Channel</p>
        //   </div>
        // ) 
      }
      {/* =========================================== */}
      {
      // (tanggal4 === "2/24/2021" || tanggal4 === "2/25/2021") && waktu4 >= 8 ? (
        <div
          className="conference-room-5"
          onClick={() => {
            unfold("room1", "Speciality Medicine");
            soundplay();
          }}
        >
          <div className="conference-bold4 Blink"></div>
          <img
            src={ellipse}
            // className="Blink"
            alt=""
            style={{ marginBottom: "15px" }}
          />
          <p className="blue-text-bg">Speciality Medicine</p>
        </div>
      // ) : tanggal4 === "2/26/2021" ? null : (
      //   <div
      //     className="conference-room-5"
      //     onClick={() => {
      //       unfold("room2", "Speciality Medicine");
      //       soundplay();
      //     }}
      //   >
      //     <div className="conference-bold4 Blink"></div>
      //     <img
      //       src={ellipse}
      //       // className="Blink"
      //       alt=""
      //       style={{ marginBottom: "15px" }}
      //     />
      //     <p className="blue-text-bg">Speciality Medicine</p>
      //   </div>
      // )
      }
      {
      // (tanggal4 === "2/24/2021" || tanggal4 === "2/25/2021") && waktu4 >= 8 ? (
        <div
          className="conference-room-6"
          onClick={() => {
            unfold("room1", "General Medicine");
            soundplay();
          }}
        >
          <div className="conference-bold5 Blink"></div>
          <img
            src={ellipse}
            // className="Blink"
            alt=""
            style={{ marginBottom: "15px" }}
          />
          <p className="blue-text-bg">General Medicine</p>
        </div>
      // ) : tanggal4 === "2/26/2021" ? null : (
      //   <div
      //     className="conference-room-6"
      //     onClick={() => {
      //       unfold("room2", "General Medicine");
      //       soundplay();
      //     }}
      //   >
      //     <div className="conference-bold5 Blink"></div>
      //     <img
      //       src={ellipse}
      //       // className="Blink"
      //       alt=""
      //       style={{ marginBottom: "15px" }}
      //     />
      //     <p className="blue-text-bg">General Medicine</p>
      //   </div>
      // )
      }
      { 
      // (tanggal4 === "2/24/2021" || tanggal4 === "1/25/2021") && waktu4 >= 8 ? (
        <div className="conference-room-7" onClick={() => {unfold("room1", "Women's Health"); soundplay()}}>
          <div className="conference-bold6 Blink"></div>
          <img
            src={ellipse}
            // className="Blink"
            alt=""
            style={{ marginBottom: "15px" }}
          />
          <p className="blue-text-bg">Women's HealthCare</p>
        </div>
      // ) : tanggal4 === "2/26/2021" ? (
      //   null
      // ) : (
      //   <div
      //     className="conference-room-7"
      //     onClick={() => {
      //       unfold("room2", "Women's Health");
      //       soundplay();
      //     }}
      //   >
      //     <div className="conference-bold6 Blink"></div>
      //     <img
      //       src={ellipse}
      //       // className="Blink"
      //       alt=""
      //       style={{ marginBottom: "15px" }}
      //     />
      //     <p className="blue-text-bg">Women's HealthCare</p>
      //   </div>
      // )
      }
      {/* =========================================== */}
      <div id="modal-container" className={`${popUp}`}>
        <div className="modal-background">
          <div className="modal">
            <div className="container-fluid full-width">
              {/* ===================== */}
              {room === "room1" ? (
                <div className="centering-bg">
                  <div className="col main-display">
                    <img
                      src={x}
                      alt="close"
                      className="x-image"
                      onClick={() => {
                        folding();
                        soundplay();
                      }}
                    />
                      {
                        roomName === "Women's Health"? (
                          <div
                            className={`text-light text-content-zoom ${textEffect}`}
                          >
                            
                            <p>
                              {roomName} <br /> 26 Februari 2021
                              </p>
             
                                <AwesomeButton
                              style={{
                                width: "246px",
                                height: "56.69px",
  
                                fontWeight: "400",
                                fontSize: "24px",
                                marginTop: "10px",
                              }}
                                href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWFlYTZkMDMtYTMxYy00NDJiLTkwNjUtMGFiMjM5NDE5NzBl%40thread.v2/0?context=%7b%22Tid%22%3a%22fcb2b37b-5da0-466b-9b83-0014b67a7c78%22%2c%22Oid%22%3a%226f9f2338-a291-4291-92d3-fce6112438c3%22%7d"
                                target="_blank"
                              >
                                ENTER
                              </AwesomeButton>
                              
                            
                          </div>
                        ) : roomName === "General Medicine" ? (
                          <div
                            className={`text-light text-content-zoom ${textEffect}`}
                          >
                          
                              <p>
                              {roomName} <br /> 26 Februari 2021
                              </p>
             
                                <AwesomeButton
                              style={{
                                width: "246px",
                                height: "56.69px",
  
                                fontWeight: "400",
                                fontSize: "24px",
                                marginTop: "10px",
                              }}
                                href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWFlYTZkMDMtYTMxYy00NDJiLTkwNjUtMGFiMjM5NDE5NzBl%40thread.v2/0?context=%7b%22Tid%22%3a%22fcb2b37b-5da0-466b-9b83-0014b67a7c78%22%2c%22Oid%22%3a%226f9f2338-a291-4291-92d3-fce6112438c3%22%7d"
                                target="_blank"
                              >
                                ENTER
                              </AwesomeButton>

                          </div>
                        ) : roomName === "Speciality Medicine" ? (
                          <div
                            className={`text-light text-content-zoom ${textEffect}`}
                          >
                        
                              <p>
                              {roomName} <br /> 26 Februari 2021
                              </p>
             
                                <AwesomeButton
                              style={{
                                width: "246px",
                                height: "56.69px",
  
                                fontWeight: "400",
                                fontSize: "24px",
                                marginTop: "10px",
                              }}
                                href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWFlYTZkMDMtYTMxYy00NDJiLTkwNjUtMGFiMjM5NDE5NzBl%40thread.v2/0?context=%7b%22Tid%22%3a%22fcb2b37b-5da0-466b-9b83-0014b67a7c78%22%2c%22Oid%22%3a%226f9f2338-a291-4291-92d3-fce6112438c3%22%7d"
                                target="_blank"
                              >
                                ENTER
                              </AwesomeButton>
                          
                          </div>
                        ) : roomName === "Radiology" ? (
                          <div
                            className={`text-light text-content-zoom ${textEffect}`}
                          >
                          
                              <p>
                                {roomName} <br /> 26 Februari 2021
                              </p>
                            
                      
                            <AwesomeButton
                            style={{
                              width: "246px",
                              height: "56.69px",

                              fontWeight: "400",
                              fontSize: "24px",
                              marginTop: "10px",
                            }}
                              href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZDEzYWIzN2YtMjM5NS00MWNiLWI5ZDktNWI1NGYzZGY3NjBk%40thread.v2/0?context=%7b%22Tid%22%3a%22fcb2b37b-5da0-466b-9b83-0014b67a7c78%22%2c%22Oid%22%3a%226f9f2338-a291-4291-92d3-fce6112438c3%22%7d"
                              target="_blank"
                            >
                              ENTER
                            </AwesomeButton>
                          </div>
                        ) : roomName === "Business Channel" ? (
                          <div
                            className={`text-light text-content-zoom ${textEffect}`}
                          >
                            
                                <p>
                                  {roomName} <br /> 26 Februari 2021
                                </p>
                              
                    
                            <AwesomeButton
                            style={{
                              width: "246px",
                              height: "56.69px",

                              fontWeight: "400",
                              fontSize: "24px",
                              marginTop: "10px",
                            }}
                              href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWFlYTZkMDMtYTMxYy00NDJiLTkwNjUtMGFiMjM5NDE5NzBl%40thread.v2/0?context=%7b%22Tid%22%3a%22fcb2b37b-5da0-466b-9b83-0014b67a7c78%22%2c%22Oid%22%3a%226f9f2338-a291-4291-92d3-fce6112438c3%22%7d"
                              target="_blank"
                            >
                              ENTER
                            </AwesomeButton>
                          </div>
                        ) : roomName === "Plenary Session" ? (
                          <div
                            className={`text-light text-content-zoom ${textEffect}`}
                          >
                            
                            <p>
                              {roomName} <br /> 23 Februari 2021
                            </p>
                    
                            <AwesomeButton
                            style={{
                              width: "246px",
                              height: "56.69px",

                              fontWeight: "400",
                              fontSize: "24px",
                              marginTop: "10px",
                            }}
                              href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWFlYTZkMDMtYTMxYy00NDJiLTkwNjUtMGFiMjM5NDE5NzBl%40thread.v2/0?context=%7b%22Tid%22%3a%22fcb2b37b-5da0-466b-9b83-0014b67a7c78%22%2c%22Oid%22%3a%226f9f2338-a291-4291-92d3-fce6112438c3%22%7d"
                              target="_blank"
                            >
                              ENTER
                            </AwesomeButton>
                          </div>
                        ) : roomName === "Cross-Functional Session" ? (
                          <div
                            className={`text-light text-content-zoom ${textEffect}`}
                          >
                            
                            <p>
                              {roomName} <br /> 26 Februari 2021
                            </p>
                    
                            <AwesomeButton
                            style={{
                              width: "246px",
                              height: "56.69px",

                              fontWeight: "400",
                              fontSize: "24px",
                              marginTop: "10px",
                            }}
                              href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWFlYTZkMDMtYTMxYy00NDJiLTkwNjUtMGFiMjM5NDE5NzBl%40thread.v2/0?context=%7b%22Tid%22%3a%22fcb2b37b-5da0-466b-9b83-0014b67a7c78%22%2c%22Oid%22%3a%226f9f2338-a291-4291-92d3-fce6112438c3%22%7d"
                              target="_blank"
                            >
                              ENTER
                            </AwesomeButton>
                          </div>
                        ) : (
                          null
                        )  
                      }
                      
                    
                  </div>
                  <div className="col bottom-display">
                    <div className="wrapper-text-bottom">
                      <h1 className={`text-light title-bottom ${textEffect}`}>
                        Anda berada di ASC Conference Hall
                      </h1>
                      <p className={`text-light text-bottom ${textEffect}`}>
                        Fokus kita tahun ini: Winning through Execution and Digital for DD Growth.
                        <br />
                        Siapkan diri kita untuk mengeksekusi strategi baru di 2021!
                      </p>
                    </div>
                  </div>
                </div>
              ) : room === "room2" ? (
                <div className="centering-bg">
                  <div className="col main-display">
                    <img
                      src={x}
                      alt="close"
                      className="x-image"
                      onClick={() => {
                        folding();
                        soundplay();
                      }}
                    />
                    <div
                      className={`text-light text-content-zoom ${textEffect}`}
                    >
                      {
                        roomName === "Women's Health" || roomName === "General Medicine" || roomName === "Speciality Medicine" || roomName === "Radiology" || roomName === "Business Channel" ? (
                          <p>
                            Meeting {roomName} belum dimulai, silahkan kembali pada 24 Februari 2021 jam 09.00
                          </p>
                        ) : (
                          <p>
                            Meeting {roomName} belum dimulai, silahkan kembali pada 26 Februari 2021 jam 09.00
                          </p>

                        )
                      }
                      
                    </div>
                  </div>
                </div>
              ) : room === "room3" ? (
                <div className="centering-bg">
                  <div className="col main-display">
                    <img
                      src={x}
                      alt="close"
                      className="x-image"
                      onClick={() => {
                        folding();
                        soundplay();
                      }}
                    />
                    <div
                      className={`text-light text-content-zoom ${textEffect}`}
                    >
                      <Slider {...configs}>
                        <div className="agenda-card">
                          <h3 className="date-title">
                            Day 1 - February 23, 2021
                          </h3>
                          <table className="table table-borderless">
                            <tbody>
                              <tr className="table-color">
                                <td>Participants Joining</td>
                                <td>08.30 - 08.45</td>
                              </tr>
                              <tr className="table-color">
                                <td>Opening</td>
                                <td>08.45 - 08.55</td>
                              </tr>
                              <tr className="table-color">
                                <td>BusinessUpdates</td>
                                <td>08.55 - 10.45</td>
                              </tr>
                              <tr className="table-color">
                                <td>Q&A Session</td>
                                <td>10.45 - 11.00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Video Competition</td>
                                <td>11.00 - 11.30</td>
                              </tr>
                              <tr className="table-color">
                                <td>Awarding Session</td>
                                <td>11.30 - 12.00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Prized Games</td>
                                <td>12.00 - 12.20</td>
                              </tr>
                              <tr className="table-color">
                                <td>Awarding Session</td>
                                <td>12.20 - 12.40</td>
                              </tr>
                              <tr className="table-color">
                                <td>Prized Games</td>
                                <td>12.40 - 13.00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Closing</td>
                                <td>13.00 - 13.30</td>
                              </tr>
                            </tbody>
                          </table>
                          <a href={agenda} download="agenda.pdf" class="download-agenda">Lihat jadwal detail</a>
                        </div>
                        <div className="agenda-card">
                          <h3 className="date-title">
                            Day 2 - February 24, 2021
                          </h3>
                          <table className="table table-borderless">
                            <tbody>
                              <tr className="table-color">
                                <td>General Medicine</td>
                                <td>08.40-15.00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Women's HealthCare</td>
                                <td>08:40-15.30</td>
                              </tr>
                              <tr className="table-color">
                                <td>Special Medicine OPTHA</td>
                                <td>08:40-15:00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Special Medicine ONCO</td>
                                <td>08:40-15:00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Business Channel</td>
                                <td>08:40-15:00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Radiology</td>
                                <td>08:40-15:30</td>
                              </tr>
                            </tbody>
                          </table>
                          <a href={agenda} download="agenda.pdf"  class="download-agenda">Lihat jadwal detail</a>
                        </div>
                        <div className="agenda-card">
                          <h3 className="date-title">
                            Day 3 - February 25, 2021
                          </h3>
                          <table className="table table-borderless">
                            <tbody>
                              <tr className="table-color">
                                <td>General Medicine</td>
                                <td>08:40-15:00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Women's HealthCare</td>
                                <td>08:40-15:00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Special Medicine OPTHAt</td>
                                <td>08:40-15:00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Special Medicine ONCO</td>
                                <td>08:40-15:00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Business Channel</td>
                                <td>08:40-16:00</td>
                              </tr>
                              <tr className="table-color">
                                <td>Radiology</td>
                                <td>08:40-16:00</td>
                              </tr>
                            </tbody>
                          </table>
                          <a href={agenda} download="agenda.pdf"  class="download-agenda">Lihat jadwal detail</a>
                        </div>
                        <div className="agenda-card">
                          <h3 className="date-title">
                            Day 4 - February 26, 2021
                          </h3>
                          <table className="table table-borderless">
                            <tbody>
                              <tr className="table-color">
                                <td>Cross Functional Presentation</td>
                                <td>09:00-15:00</td>
                              </tr>
                            </tbody>
                          </table>
                          <a href={agenda} download="agenda.pdf"  class="download-agenda">Lihat jadwal detail</a>
                        </div>
                      </Slider>
                    </div>
                  </div>
                </div>
              ) : null}

              {/* ======================= */}
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
}

export default Conference;
